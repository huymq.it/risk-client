package com.lvt.olympus.risk.entity;

import lombok.Data;

@Data
public class RiskAction {
	private int version;
	private String type;
	private Body body;

	@Data
	public class Activity {
		private String actionId;
		private String actionCode;
		private String actionName;
		private String productId;
		private String productCode;
		private String categoryId;
		private String categoryCode;
	}

	@Data
	public class Body {
		private Device device;
		private Client client;
		private Service service;
		private Transaction transaction;
		private Activity activity;
	}

	@Data
	public class Client {
		private String userId;
		private String userName;
		private String customerNo;
		private String sessionId;
		private String ipAddress;
		private String longitude;
		private String latitude;
		private String clientRequestId;
		private String clientTime;
		private String channelCode;
	}

	@Data
	public class Device {
		private String deviceId;
		private String name;
		private String platform;
		private String platformVersion;
		private String appVersion;
	}

	@Data
	public class Service {
		private String system;
		private String service;
		private String node;
		private String function;
		private String resultCode;
		private String resultDesc;
		private String requestId;
		private long systemTime;
		private int cost;
	}

	@Data
	public class Transaction {
		private TransactionHeader transactionHeader;
		private TransactionFrom transactionFrom;
		private TransactionTo transactionTo;
		private TransactionAmount transactionAmount;
		private TransactionFee transactionFee;
		private TransactionCommission transactionCommission;
		private TransactionPromotion transactionPromotion;
		private TransactionInfo transactionInfo;
		private TransactionResponse transactionResponse;
		private TransactionStatus transactionStatus;
		private TransactionBill transactionBill;
	}

	@Data
	public class TransactionAmount {
		private String amountTransfer;
		private String amountCurrency;
		private String amountRate;
		private String discountAmount;
		private String vatAmount;
		private String inputAmount;
		private String partnerDiscountAmount;
	}

	@Data
	public class TransactionBill {
		private String billPayStatus;
	}

	@Data
	public class TransactionCommission {
		private String commissionAmount;
		private String commissionCurrency;
		private String commissionRate;
		private String commissionInd;
	}

	@Data
	public class TransactionFee {
		private String feeAmount;
		private String feeCurrency;
		private String feeAmountRate;
		private String feeInd;
	}

	@Data
	public class TransactionFrom {
		private String fromSystemId;
		private String fromCustNo;
		private String fromAccountClass;
		private String fromAccountType;
		private String fromAccountNo;
		private String fromAccountBranch;
		private String fromAccountCurrency;
		private String fromAccountFullname;
		private String fromMessage;
	}

	@Data
	public class TransactionHeader {
		private String rowId;
		private String transId;
		private String transCode;
		private String userId;
		private String userName;
		private String transTime;
		private String actionId;
		private String transType;
		private String channelReceiver;
		private String stakUserTypeDo;
		private String userDo;
		private String branchCodeDo;
		private String productId;
		private String businessProcess;
		private String originalRequestId;
		private String makerId;
		private String makerDate;
		private String checkerDate;
		private String requestId;
		private String sourceType;
		private String categoryCode;
		private String custNoDo;
	}

	@Data
	public class TransactionInfo {
		private String transName;
		private String transDesc;
		private String requestTime;
		private String requestChannel;
		private String transRequest;
		private String coreSystem;
		private String receiveTime;
		private String insertTime;
		private String finishTime;
		private String inputType;
		private String inputValue;
	}

	@Data
	public class TransactionPromotion {
		private String promotionAmount;
		private String promotionCurrency;
		private String promotionRate;
	}

	@Data
	public class TransactionResponse {
		private String responseTime;
		private String responseCode;
		private String responseMessage;
		private String responseStatus;
		private String errorCode;
	}

	@Data
	public class TransactionStatus {
		private String approveStatus;
		private String transStatus;
		private String transProgress;
		private String paymentStatus;
	}

	@Data
	public class TransactionTo {
		private String toSystemId;
		private String toCustNo;
		private String toAccountNo;
		private String toAccountCurrency;
		private String toAmount;
		private String toAccountFullname;
		private String toUserName;
	}
}
