package com.lvt.olympus.risk.client;

import com.lvt.olympus.risk.entity.RiskAction;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;

public class RiskClient {
	private String riskEngineUrl;

	public RiskClient(String riskEngineUrl) {
		Unirest.config().reset();
		this.riskEngineUrl = riskEngineUrl;
	}
	
	public RiskClient(String riskEngineUrl, int timeoutMilisecond) {
		Unirest.config().reset();
		Unirest.config().socketTimeout(timeoutMilisecond).connectTimeout(timeoutMilisecond);
		this.riskEngineUrl = riskEngineUrl;
	}

	public Boolean check(RiskAction action, String ruleCode) {
		HttpResponse<JsonNode> response = Unirest.post(riskEngineUrl  + "/check/" + ruleCode + "/rule")
				.header("Content-Type", "application/json")
				.body(action).asJson();
		
		if (response.getStatus() == 200) {
			JSONObject obj = response.getBody().getObject();
			
			if(obj.has(ruleCode)) {
				return obj.getBoolean(ruleCode);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public String check(String action) {
		HttpResponse<String> response = Unirest.post(riskEngineUrl + "/check/all")
				.header("Content-Type", "application/json")
				.header("accept", "application/json").body(action).asString();

		if (response.getStatus() == 200) {
			return response.getBody();
		} else {
			return null;
		}
	}
	
	public Boolean check(String action, String ruleCode) {
		HttpResponse<JsonNode> response = Unirest.post(riskEngineUrl + "/check/" + ruleCode + "/rule")
				.header("Content-Type", "application/json")
				.header("accept", "application/json").body(action).asJson();

		if (response.getStatus() == 200) {
			JSONObject obj = response.getBody().getObject();
			
			if(obj.has(ruleCode)) {
				return obj.getBoolean(ruleCode);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
}
